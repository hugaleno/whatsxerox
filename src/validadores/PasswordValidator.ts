import { AbstractControl } from "@angular/forms";

export class PasswordValidator {
    //Por convenção dos validators do angular, esse método deve retornar null quando estiver tudo OK.
    static matchPassword(control: AbstractControl){
        let password = control.get("senha").value;
        let confirmPassword = control.get("confirmaSenha").value;

        if(password != confirmPassword){
            control.get("confirmaSenha").setErrors({MatchPassword: true});
        }else{
            return null;
        }
    }
}