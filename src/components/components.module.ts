import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { PipesModule } from './../pipes/pipes.module';
import { CaixaConversaComponent } from './caixa-conversa/caixa-conversa';
import { CaixaMensagemComponent } from './caixa-mensagem/caixa-mensagem';
import { CustomHeadComponent } from './custom-head/custom-head';
@NgModule({
	declarations: [
		CustomHeadComponent,
		CaixaMensagemComponent,
    	CaixaConversaComponent
	],
	imports: [
		IonicModule,
		CommonModule,
		PipesModule
	],
	exports: [
		CustomHeadComponent,
		CaixaMensagemComponent,
    	CaixaConversaComponent
	]
})
export class ComponentsModule { }
