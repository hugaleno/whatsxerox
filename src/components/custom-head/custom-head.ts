import { Component, Input, OnInit } from '@angular/core';
import { AlertController, App, ModalController, NavController } from 'ionic-angular';
import { LoginPage } from '../../pages/login/login';
import { AdicionarContatoModalPage } from './../../pages/adicionar-contato-modal/adicionar-contato-modal';
import { AutenticacaoProvider } from './../../providers/autenticacao/autenticacao';

@Component({
  selector: 'custom-head',
  templateUrl: 'custom-head.html'
})
export class CustomHeadComponent implements OnInit {

  @Input() titulo: string;
  @Input() exibirIcones: boolean = true;
  private navCtrl: NavController;

  constructor(
    private modalCtrl: ModalController,
    private auth: AutenticacaoProvider,
    private app: App,
    private alertCtrl: AlertController
  ) { }

  ngOnInit(): void {
    this.navCtrl = this.app.getRootNav();
  }
  //Método responsável por exibir o modal com a página de Adicionar contato
  abrirModal(): void {
    //Cria um modal e o exibe
    this.modalCtrl.create(AdicionarContatoModalPage).present();
  }

  //Método responsável por deslogar o usuário da aplicação
  deslogar(): void {
    //Cria um alerta de confirmação antes de deslogar
    this.alertCtrl.create({
      message: "Você deseja realmente sair?",
      buttons: [
        {
          text: "Sim",
          handler: () => {//Ação que será executada assim que o usuário clicar no botão Sim
            this.auth.fazerLogoff()
              .then(() => {
                this.navCtrl.setRoot(LoginPage);
              });
          }
        },
        {
          text: "Não"
        }
      ]
    }).present();

  }

}
