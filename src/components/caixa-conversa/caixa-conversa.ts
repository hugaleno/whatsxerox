import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { User } from '../../model/user.model';
import { Conversa } from './../../model/conversa.model';
import { ConversaPage } from './../../pages/conversa/conversa';

@Component({
  selector: 'caixa-conversa',
  templateUrl: 'caixa-conversa.html'
})
export class CaixaConversaComponent {

  @Input() conversa: Conversa;

  constructor(
    private navCtrl: NavController
  ) {
  }

  //Navegar para página de conversa
  continuarConversa(): void {
    //Cria um contato para passar como parâmetro para Pagina de Conversa
    const contato = new User(this.conversa.titulo, this.conversa.email);
    //Navega para a página e encaminha o objeto contato como parâmetro
    this.navCtrl.push(ConversaPage, {
      contato: contato
    });
  }

}
