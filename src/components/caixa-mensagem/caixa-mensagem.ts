import { Component, Input } from '@angular/core';
import { Mensagem } from './../../model/mensagem.model';

@Component({
  selector: 'caixa-mensagem',
  templateUrl: 'caixa-mensagem.html',
  //Alinha a mensagem a esquerda ou a direita, dependendo de quem enviou a mensagem remetente/destinario
  host: {
    '[style.justify-content]': 
      '(mensagem.tipo =="e" ? "flex-end" : "flex-start")',
    '[style.text-align]':
      '(mensagem.tipo =="e" ? "right": "left")',
    '[style.margin-left]':
      '(mensagem.tipo=="e" ? "40px": "0px")',
    '[style.margin-right]':
      '(mensagem.tipo=="e" ? "0px": "40px")'
  }
})
export class CaixaMensagemComponent {
  @Input() mensagem: Mensagem;
  
  constructor() {
  }

}
