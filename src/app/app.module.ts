import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ContatoProvider } from '../providers/contato/contato.provider';
import { ConversasProvider } from '../providers/conversas/conversas.provider';
import { MensagensProvider } from '../providers/mensagens/mensagens.provider';
import { PagesModule } from './../pages/pages.module';
import { AutenticacaoProvider } from './../providers/autenticacao/autenticacao';
import { UserProvider } from './../providers/user/user.provider';
import { MyApp } from './app.component';
import { firebaseAppConfig } from './constantes';



@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseAppConfig),//Adição da biblioteca angularfire2
    AngularFireDatabaseModule,//Adição da biblioteca angularfire2
    AngularFireAuthModule,//Adição da biblioteca angularfire2
    PagesModule //Importação do PagesModule que é o módulo que contém os screens do app
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AutenticacaoProvider,
    UserProvider,
    ContatoProvider,
    ConversasProvider,
    MensagensProvider
  ]
})
export class AppModule {}
