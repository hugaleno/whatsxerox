import { NgModule } from '@angular/core';
import { CortaUltimaMensagemPipe } from './corta-ultima-mensagem/corta-ultima-mensagem';
@NgModule({
	declarations: [CortaUltimaMensagemPipe],
	imports: [],
	exports: [CortaUltimaMensagemPipe]
})
export class PipesModule {}
