import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'cortaUltimaMensagem',
})
export class CortaUltimaMensagemPipe implements PipeTransform {
  
  transform(ultimaMensagem: string, quantCaracteres: number) {
    if(ultimaMensagem.length <= quantCaracteres){ //Se a mensagem for pequena não a necessidade de fazer a adição dos 3 pontos.
      return ultimaMensagem;
    }
    return `${ultimaMensagem.substring(0, quantCaracteres)}...`;//Mensagem grande eh cortada para ser exibida na tela de conversas
  }
}
