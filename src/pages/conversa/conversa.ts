import { Component, ViewChild } from '@angular/core';
import { Content, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/operator/first';
import { Mensagem } from '../../model/mensagem.model';
import { ConversasProvider } from '../../providers/conversas/conversas.provider';
import { User } from './../../model/user.model';
import { MensagensProvider } from './../../providers/mensagens/mensagens.provider';

@Component({
  selector: 'page-conversa',
  templateUrl: 'conversa.html',
})
export class ConversaPage {

  contato: User;
  mensagens: Observable<Mensagem[]>;
  //Pega uma instância do content
  @ViewChild(Content) content: Content;
  //Referencia para fazer a desinscrição depois
  inscricao: Subscription;

  constructor(
    public navParams: NavParams,
    public mensagensProvider: MensagensProvider,
    public conversasProvider: ConversasProvider
  ) {
    this.contato = navParams.get("contato");
    mensagensProvider.inicializaReferencias(this.contato);
    conversasProvider.inicializaReferencias(this.contato);
  }

  deslizaPraBaixo(): void {
    setTimeout(() => {
      //Verifica se há necessidade do scroll 
      if (this.content._scroll) {
        //Faz a rolagem para baixo
        this.content.scrollToBottom();
      }
    }, 300)

  }

  ionViewDidLoad(): void {
    this.mensagens = this.mensagensProvider.getMensagens();
    //Sempre que chegar uma nova mensagem o método deslizaPraBaixo 
    //é chamado para fazer a rolagem automática
    this.inscricao = this.mensagens.subscribe(() => {
      this.deslizaPraBaixo();
    })
  }
  ionViewWillUnload(): void {
    //Ao sair da aplicação remove a inscrição no observable
    this.inscricao.unsubscribe();
  }

  enviarMensagem(msgTexto: string): void {
    this.mensagensProvider.enviarMensagem(msgTexto);
  }

}
