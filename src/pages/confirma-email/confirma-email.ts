import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AutenticacaoProvider } from '../../providers/autenticacao/autenticacao';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-confirma-email',
  templateUrl: 'confirma-email.html',
})
export class ConfirmaEmailPage {

  email: string;
  doLogin: string;
  mensagem: string = "";
  btnReenviarDisabled: boolean = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public auth: AutenticacaoProvider
  ) {
    //Recebendo o parâmetro email que foi enviado para essa pagina da tela de cadastro
    this.email = navParams.get("email");
    if(navParams.get("doLogin")){
      this.doLogin = "true";
    }
  }
  //Reenvia email de verificação
  reenviarEmailVerificacao(): void {
    this.btnReenviarDisabled = true;
    this.auth.reenviarEmailVerificacao()
      .then(()=> {
        this.btnReenviarDisabled = false;
        this.mensagem = `E-mail de verificação enviado para ${this.email}`
      })
      .catch( (error: any) => {
        this.btnReenviarDisabled = false;
        this.mensagem = error.message;
      });
  }

  navLogin():void {
    this.navCtrl.setRoot(LoginPage);
  }


}
