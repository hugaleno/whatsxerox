import { Component } from "@angular/core";
import { ConversasPage } from "../conversas/conversas";
import { ContatosPage } from './../contatos/contatos';

@Component({
    selector: 'page-tabs',
    templateUrl: './tabs.html'
})
export class TabsPage {
    tab1 = ConversasPage;
    tab2 = ContatosPage;
}