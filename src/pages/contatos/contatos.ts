import { Component } from '@angular/core';
import { ModalController, NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { User } from './../../model/user.model';
import { ContatoProvider } from './../../providers/contato/contato.provider';
import { AdicionarContatoModalPage } from './../adicionar-contato-modal/adicionar-contato-modal';
import { ConversaPage } from './../conversa/conversa';


@Component({
  selector: 'page-contatos',
  templateUrl: 'contatos.html',
})
export class ContatosPage {

  contatos: Observable<User[]>

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public contatoProvider: ContatoProvider
  ) {
    //Atualiza as referencias quando o usuário logar no sistema, evitando o problema de um logar/deslogar/logar com outra conta
    this.contatoProvider.inicializa()
      .then(()=>{
        //Atualiza a lista de contato após as atualizações das referências já terem sido feitas
        this.contatos = contatoProvider.contatos;
      });
    
  }

  abrirAddContatoModal(): void {
    this.modalCtrl.create(AdicionarContatoModalPage).present();
  }

  //Inicia a conversa com o contato escolhido
  iniciarConversa(contato: User): void {
    this.navCtrl.push(ConversaPage, {
    //Chave  :  Valor
      contato:contato
    });
  }

}
