import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ComponentsModule } from '../components/components.module';
import { AdicionarContatoModalPage } from './adicionar-contato-modal/adicionar-contato-modal';
import { CadastroPage } from './cadastro/cadastro';
import { ConfirmaEmailPage } from './confirma-email/confirma-email';
import { ContatosPage } from './contatos/contatos';
import { ConversaPage } from './conversa/conversa';
import { ConversasPage } from './conversas/conversas';
import { LoginPage } from './login/login';
import { TabsPage } from './tabs/tabs';

@NgModule({
    declarations:[
        LoginPage,
        CadastroPage,
        TabsPage,
        ContatosPage,
        ConversasPage,
        AdicionarContatoModalPage,
        ConversaPage,
        ConfirmaEmailPage
    ],
    imports:[
        IonicModule,
        CommonModule,
        ComponentsModule
    ],
    exports: [
    ],
    entryComponents: [
        LoginPage,
        CadastroPage,
        TabsPage,
        ContatosPage,
        ConversasPage,
        AdicionarContatoModalPage,
        ConversaPage,
        ConfirmaEmailPage
    ]
})
export class PagesModule {}