import { ConversasProvider } from './../../providers/conversas/conversas.provider';
import { Observable } from 'rxjs/Observable';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Conversa } from '../../model/conversa.model';

@Component({
  selector: 'page-conversas',
  templateUrl: 'conversas.html',
})
export class ConversasPage {
  conversas: Observable<Conversa[]>;

  constructor(
    public navCtrl: NavController,
    private conversasProvider:ConversasProvider
  ) {
    //Atualiza as referencias quando o usuário logar no sistema, evitando o problema de um logar/deslogar/logar com outra conta
    this.conversasProvider.inicializaReferencias()
    .then((data)=> {
      //Atualiza as conversas após a atualização das referências
      if(data){
        this.conversas = this.conversasProvider.getConversas();
      }
    });
  }

  ionViewDidLoad():void {
    
  }
}
