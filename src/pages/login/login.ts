import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import firebase from 'firebase';
import { Loading, LoadingController, NavController } from 'ionic-angular';
import { ConfirmaEmailPage } from '../confirma-email/confirma-email';
import { AutenticacaoProvider } from './../../providers/autenticacao/autenticacao';
import { CadastroPage } from './../cadastro/cadastro';
import { TabsPage } from './../tabs/tabs';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  
  formLogin: FormGroup;
  erroEncontrado: string = "";

  constructor(
    public navCtrl: NavController, 
    public formBuilder: FormBuilder,
    public auth: AutenticacaoProvider,
    public loadingCtrl: LoadingController
  ) {
    //Faz a validação do formulário
    let mailRegex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i
    this.formLogin = formBuilder.group({
      email:['', [Validators.required, Validators.pattern(mailRegex)]],
      senha:['', [Validators.required, Validators.minLength(6)]]
    });
  }
  //Navegar para tela de cadastro
  goCadastro(){
    this.navCtrl.push(CadastroPage);
  }
  entrar(): void{
    this.navCtrl.push(TabsPage);
  }
  //Efetuar Login
  login(): void {
    let loading = this.showLoading("Efetuando login...");
    const { email, senha } = this.formLogin.value;
    this.auth.loginEmailESenha(email, senha)
      .then( (user:firebase.User)=> {
        loading.dismiss();
        this.erroEncontrado = "";
        if(user){
          this
          .navCtrl
          .push(
            user.emailVerified ? TabsPage : ConfirmaEmailPage,//Verifica se a conta de email já está verificada, 
                                                              //se estiver carrega a pagina principal, senão carrega a página de confirmação do email
            {email: email, doLogin: "true"}
          );
        }
        
      })
      .catch( (erro:any) => {
        loading.dismiss();
        this.erroEncontrado = this.traduzErroLogin(erro.code, erro.message);//Adiciona a mensagem de erro para notificar o usuario do problema
        //console.log(erro);
      });
    
  }
  traduzErroLogin(errorCode: string, errorMessage: string): string {
    switch(errorCode){
      case "auth/user-disabled":
        return "Conta desativada. " +
        "Mande um email para support@meuapp.com para reativar sua conta.";

      case "auth/invalid-email":
        return "E-mail inválido";

      case "auth/user-not-found":
        return "Usuário não encontrado." +
        "Verifique seu email ou faça seu cadastro.";

      case "auth/wrong-password":
        return "E-mail ou senha inválido!";

      default:
        return errorMessage;
    }
  }

  showLoading(mensagem: string): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content:mensagem
    });
    loading.present();
    return loading;
  }

}
