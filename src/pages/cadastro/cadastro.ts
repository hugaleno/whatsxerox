import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import firebase from 'firebase';
import { AlertController, Loading, LoadingController, NavController } from 'ionic-angular';
import { ConfirmaEmailPage } from '../confirma-email/confirma-email';
import { AutenticacaoProvider } from './../../providers/autenticacao/autenticacao';
import { UserProvider } from './../../providers/user/user.provider';
import { PasswordValidator } from './../../validadores/PasswordValidator';

@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

  cadastroForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public auth: AutenticacaoProvider,
    public userProvider: UserProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController
  ) {
    const mailRegex = /^[a-z20-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i
    this.cadastroForm = formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3)]],
      email: ['', Validators.compose([Validators.required, Validators.pattern(mailRegex)])],
      senha: ['', [Validators.required, Validators.minLength(6)]],
      confirmaSenha: ['', Validators.required]
      
    }, {validator: PasswordValidator.matchPassword});
  }
  onSubmit(): void {
    let loading: Loading = this.showLoading("Cadastrando...");
    //pega as informações que foram preenchidas no formulário.
    let formUser = this.cadastroForm.value;
    //Cria o usuario na base de autenticação do firebase
    this.auth.criarUsuarioAutenticado(formUser.email, formUser.senha)
      .then((user: firebase.User) => {
        //Envia email de verificação
        user.sendEmailVerification()
          .then( () => console.log("Email enviado"))
          .catch( (error:any) => console.log("Erro", error.message));
        //Cadastra o usuario na base de dados.
        this.userProvider.cadastrarUsuario(formUser)
          .then(() => {
            loading.dismiss();

             loading= this.showLoading("Cadastrado com sucesso. Redirecionando...");
            setTimeout(()=>{
              loading.dismiss();                    //Chave   valor
              this.navCtrl.setRoot(ConfirmaEmailPage,{email: formUser.email});
            },1000); 
            
          })
          .catch(() => {
            loading.dismiss();
            
          });
      })
      .catch((erro:any) => {
        loading.dismiss();
        this.showAlert(this.traduzErros(erro.code, erro.message));
        //console.log("Erro", erro);
      });
  }

  showLoading(mensagem: string): Loading {
    let loading: Loading;
    
    loading = this.loadingCtrl.create({
      content: mensagem
    });

    loading.present();

    return loading;
  }

  showAlert(mensagem:string): void {
    this.alertCtrl.create({
      message: mensagem,
      title:'Erro',
      buttons:['Ok']
    }).present();
  }

  traduzErros(errorCode: string, errorMessage: string): string {
    switch(errorCode){
      case "auth/email-already-in-use":
        return "E-mail já está sendo utilizado.";

      case "auth/invalid-email":
        return "E-mail inválido";

      case "auth/operation-not-allowed":
        return "Operação não permitida";

      case "auth/weak-password":
        return "Senha fraca!";

      default:
        return errorMessage
    }
  }
}
