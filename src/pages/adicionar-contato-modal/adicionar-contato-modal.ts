import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, ToastController, ViewController } from 'ionic-angular';
import 'rxjs/add/operator/first';
import { ContatoProvider } from '../../providers/contato/contato.provider';
import { AutenticacaoProvider } from './../../providers/autenticacao/autenticacao';
import { UserProvider } from './../../providers/user/user.provider';

@Component({
  selector: 'page-adicionar-contato-modal',
  templateUrl: 'adicionar-contato-modal.html',
})
export class AdicionarContatoModalPage {
/* 
  Classe responsável pela adição de um novo contato
*/
  addContatoForm: FormGroup;

  constructor(
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    public userProvider: UserProvider,
    public contatoProvider: ContatoProvider,
    public auth: AutenticacaoProvider,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController
  ) {
    const mailRegex = /^[a-z20-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i
    //Adição da validação do formulário
    this.addContatoForm = formBuilder.group({
      email: ['', [Validators.required, Validators.pattern(mailRegex)]]
    });
  }

  //Esconde o modal ao ser chamado
  dismiss(): void {
    this.viewCtrl.dismiss();
  }

  addContato(): void {
    //console.log(this.addContatoForm);
    //Destructing ES6, extrai o campo email do addContatoForm value
    const { email } = this.addContatoForm.value;
    const currentUser = this.auth.currentUser;

    //Não permite que o usuário adicione ele mesmo como contato
    if (email === currentUser.email) {
      this.showAlert("Você não pode se auto adicionar a si mesmo!");
      return;
    }

    this.contatoProvider
      .isContatoJaAdicionado(email) //Verifica se o contato que está sendo adicionado já não está na lista de contatos.
      .first() //Escuta somente uma vez no observable
      .subscribe((estaAdicionado: boolean) => {
        if (estaAdicionado) {
          this.showAlert("Este usuário já está na sua lista de contatos.");
        } else {
          //Buscar nome do usuário para que seja passado como parâmetro junto com o email
          this.userProvider.getUserNomeByEmail(email)
            .valueChanges()
            .first()
            .subscribe((data: any) => {
              if (data) {
                this.contatoProvider
                  .addUsuarioContato(data.nome, email)
                  .then(() => {
                    this.showToast(`Contato ${email} adicionado!`);
                    this.dismiss();
                  })
                  .catch((erro: Error) => {
                    this.showAlert(erro.message);
                  });
                /*  let user: User = new User(data.nome, email);
                 this.contatoProvider.addUsuarioContato2(user);
                 this.contatoProvider.addUsuarioContato3(user); */
              }else{
                this.showAlert("Usuário não encontrado! :(");
              }
            });

        }
      })
  }

  showToast(mensagem: string): void {
    this.toastCtrl.create({
      message: mensagem,
      position: "bottom",
      duration: 3000
    }).present();
  }

  showAlert(mensagem: string): void {
    this.alertCtrl.create({
      message: mensagem,
      title: "Erro",
      buttons: ['Ok']
    }).present();
  }
}
