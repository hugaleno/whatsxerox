import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import b64 from 'base-64';
import { FIREBASE_CONTATO_URL } from './../../app/firebaseUrls';

@Injectable()
export class UserProvider {

  constructor(
    public afDB: AngularFireDatabase
  ) {
  }
//cadastra o usuario na database
  cadastrarUsuario( userInfo: {name: string, email:string}): Promise<void> {
    const emailB64 = b64.encode(userInfo.email);                  //Utiliza o b64 para codificar o email e usá-lo como identificado unico no firebase.
    return this.afDB.object(`${FIREBASE_CONTATO_URL}/${emailB64}`)// o próprio email não é utilizado porque contém caracteres especiais que não são permitidos pelo firebase na chave de identificação
      .set({nome: userInfo.name});
  }

  //Busca o nome do usuário pelo email
  getUserNomeByEmail( email: string ): AngularFireObject<{}>{
    const emailB64 = b64.encode(email);
    return this.afDB.object(`${FIREBASE_CONTATO_URL}/${emailB64}`);
  }

}
