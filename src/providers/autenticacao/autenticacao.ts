import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase';
import { UserProvider } from '../user/user.provider';

@Injectable()
export class AutenticacaoProvider {

  currentUser: firebase.User;
  constructor(
    public afAU: AngularFireAuth,
    public userProvider: UserProvider
  ) { 
    this.getCurrentUser();
  }
//Criar usuario na base de Autentication do firebase. 
  criarUsuarioAutenticado(email: string, senha: string): Promise<any> {
    return this
      .afAU
      .auth
      .createUserWithEmailAndPassword(email, senha);
  }
//Faz o login do usuário usando o email e senha no firebase
  loginEmailESenha(email: string, senha: string): Promise<any> {
    return this
      .afAU
      .auth
      .signInWithEmailAndPassword(email, senha);
  }

  //Monitora o estado do usuaŕio que está logado, caso o usuário deslogue e logue com outro usuário essa alteração será detectada
  private getCurrentUser(): void {
    this.afAU.authState.subscribe((user:firebase.User) => {
      //console.log(user);
      if(!user){
        return;
      }
      //console.log("Usuario", user);
      this.currentUser = user;
      this.userProvider.getUserNomeByEmail(user.email)
        .valueChanges()
        .first()
        .subscribe( (data:any) => {
          if(data){
            user.updateProfile({displayName:data.nome, photoURL:''});
          }
        });
      
    });
  }

  reenviarEmailVerificacao(): Promise<any> {
    return this.currentUser.sendEmailVerification();
  }

  fazerLogoff(): Promise<any> {
    return this.afAU.auth.signOut();
  }

}
