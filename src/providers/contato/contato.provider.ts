import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import b64 from 'base-64';
import firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { FIREBASE_USUARIO_CONTATO } from '../../app/firebaseUrls';
import { User } from './../../model/user.model';
import { AutenticacaoProvider } from './../autenticacao/autenticacao';

@Injectable()
export class ContatoProvider {

  private contatosRef: AngularFireList<User>;
  public contatos: Observable<User[]>;

  constructor(
    private afDB: AngularFireDatabase,
    private auth: AutenticacaoProvider
  ) {
    this.inicializa();
  }
  /* Inicializa uma referência para o nó que conterá os contatos do usuário
  contatosRef guarda a referencia para esse nó, enquanto o contatos 
  é um Observable para a lista de contatos, sempre que houver alteração
  nessa lista, todos que tiverem inscritos receberão as alterações.
  */
  inicializa(): Promise<boolean> {
    if (!this.auth.currentUser) return Promise.reject(false);

    let currentUser: firebase.User = this.auth.currentUser;
    const emailB64 = b64.encode(currentUser.email);
    this.contatosRef = this.afDB.list(`${FIREBASE_USUARIO_CONTATO}/${emailB64}`);
    this.contatos = this.contatosRef.valueChanges();
    return Promise.resolve(true);
  }
  //Primeira forma de adicionar um contato
  addUsuarioContato(nome: string, email: string): Promise<any> {
    return this.contatosRef
      .push({ nome: nome, email: email })
      .once('value');
  }
  //Segunda forma de adicionar um contato
  addUsuarioContato2(user: User): Promise<any> {
    return this.contatosRef
      .push({ nome: user.nome, email: user.email })
      .once('value');
  }
  //Terceira forma de adicionar um contato
  addUsuarioContato3(user: User): Promise<any> {
    return this.contatosRef
      .push(user)
      .once('value');
  }

  /* 
    Checa se o usuário já está na lista 
    de contatos.Retorna um Observable com um boolean indicando se o usuário
    está ou não na lista de contatos
    true = já está na lista
    false = não está na lista
  */
  isContatoJaAdicionado(email): Observable<boolean> {
    return this.contatos.map((users: User[]) => {

      return users.filter(
        (user: User) => user.email === email).length > 0;

      /*  let usuarios = users.filter( 
         (user: User) => user.email === email);

         return usuarios.length > 0; */
    })
  }

}
