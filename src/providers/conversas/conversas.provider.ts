import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import b64 from 'base-64';
import firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { FIREBASE_USUARIO_CONVERSAS } from './../../app/firebaseUrls';
import { Conversa } from './../../model/conversa.model';
import { User } from './../../model/user.model';
import { AutenticacaoProvider } from './../autenticacao/autenticacao';

@Injectable()
export class ConversasProvider {

  private conversasRemetente: AngularFireList<Conversa>;
  private conversasDestinatario: AngularFireList<Conversa>;
  private destinatario: User;

  constructor(
    private afDB: AngularFireDatabase,
    private auth: AutenticacaoProvider
  ) {
    this.inicializaReferencias();
  }
  //A interrogação indica parâmetro Opcional
  inicializaReferencias(contato?: User): Promise<boolean> {
    //Se não tiver usuário logado, não continua
    if (!this.auth.currentUser) {
      return Promise.reject(false);
    }

    const remetenteEmailB64 =
      b64.encode(this.auth.currentUser.email);

    // Iniciar a referencia para as conversas do remetente e organiza a ordem pelo atributo timestamp. Mais Antigo ---> Mais recente
    this.conversasRemetente = this.afDB.list(
      `${FIREBASE_USUARIO_CONVERSAS}/${remetenteEmailB64}`,
      ref => ref.limitToLast(30).orderByChild('timestamp')
    );

    /* Se receber um contato como parâmetro inicia a referencia para as conversas do contato. 
    * Quando o remetente inicia uma conversa, é necessário iniciar uma conversa com o usuário destinatário, 
    * para que este tenha condições de saber quando alguém estiver conversando com ele. 
    */
    if (contato) {
      this.destinatario = contato;
      const destinatarioEmailB64 =
        b64.encode(contato.email);
      this.conversasDestinatario = this.afDB.list(
        `${FIREBASE_USUARIO_CONVERSAS}/${destinatarioEmailB64}`
      );
    }
    return Promise.resolve(true);

  }

  /* 
  * Criar uma nova conversa.
  *  @param textoMensagem  Texto que será utilizado como Ultima mensagem da conversa. 
  * É exibido na tela de conversas, para cada conversa, a ultima mensagem trocada.
  */
  criarConversa(textoMensagem: string): Promise<void> {
    const emailRemetenteB64 = b64.encode(this.auth.currentUser.email);
    const emailDestinarioB64 = b64.encode(this.destinatario.email);
    const timestamp = firebase.database.ServerValue.TIMESTAMP;
    //Cria uma conversa
    let conversa: Conversa =
      new Conversa(
        this.destinatario.nome,
        this.destinatario.email,
        textoMensagem,
        timestamp
      );
    //Adiciona uma conversa na referencia do remetente
    return this.conversasRemetente.set(emailDestinarioB64, conversa)
      .then(() => {
        //Altera os atributos email e titulo para adicionar a mesma conversa na referência do destinario
        //Usuario A fala com B e o Usuario B fala com A, para o usuario A o titulo da conversa deve ser o nome de B,
        //enquanto o titulo da conversa para B deve ser o nome de A.
        conversa.email = this.auth.currentUser.email;
        conversa.titulo = this.auth.currentUser.displayName;
        this.conversasDestinatario.set(emailRemetenteB64, conversa);
      });
  }

  //Retorna as conversas
  getConversas(): Observable<Conversa[]> {
    if (this.conversasRemetente) {
      return this.conversasRemetente
        .valueChanges()
        .map((conversas: Conversa[]) => {
          //Inverte a ordem, dessa forma as conversas aparecerão na ordem Mais Recente ---> Mais antiga, mantendo sempre as ultimas conversas no topo.
          return conversas.reverse();
        });
    }

  }

}
