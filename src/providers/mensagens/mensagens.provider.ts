import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import b64 from 'base-64';
import firebase from 'firebase';
import { Observable } from 'rxjs/Observable';
import { FIREBASE_MENSAGENS } from './../../app/firebaseUrls';
import { Mensagem } from './../../model/mensagem.model';
import { User } from './../../model/user.model';
import { AutenticacaoProvider } from './../autenticacao/autenticacao';
import { ConversasProvider } from './../conversas/conversas.provider';


@Injectable()
export class MensagensProvider {

  currentUser: firebase.User
  mensagensRemetente: AngularFireList<Mensagem>;
  mensagensDestinario: AngularFireList<Mensagem>;

  constructor(
    private afDB: AngularFireDatabase,
    private auth: AutenticacaoProvider,
    private conversasProvider: ConversasProvider
  ) {
    this.currentUser = auth.currentUser;
  }
  //Inicializa as referencias dos dois usuário que estão trocando as mensagens
  inicializaReferencias(contato: User): void {
    if(contato){
      const emailRemetenteB64 = b64.encode(this.currentUser.email);
      const emailDestinarioB64 = b64.encode(contato.email);
      this.mensagensRemetente = 
        this.afDB
          .list(
            `${FIREBASE_MENSAGENS}/${emailRemetenteB64}/${emailDestinarioB64}`
          );
      this.mensagensDestinario =
        this.afDB
          .list(
            `${FIREBASE_MENSAGENS}/${emailDestinarioB64}/${emailRemetenteB64}`
          );
    }
  }

  //Envia a mensagem 
  enviarMensagem( msgTxt: string ): void {
    const timestamp = firebase.database.ServerValue.TIMESTAMP;
    let mensagem: Mensagem = new Mensagem(msgTxt,timestamp);
    mensagem.tipo = "e";//Marca essa mensagem como sendo enviada, nesse caso nas conversas do remetente(quem enviou a mensagem), ela estará marcada com e.
    this.mensagensRemetente.push(mensagem)
      .then( () => {
        mensagem.tipo = "r"//Marca essa mensagem como sendo recebida, nesse caso nas conversas do destinário(quem recebeu a mensagem), ela estará marcada com r.
        this.mensagensDestinario.push(mensagem)
        .then(()=>{
          //Executa esse método para criar a conversa que deverá sempre conter a ultima mensagem trocada na conversa
          this.conversasProvider.criarConversa(msgTxt);
        });
      });  
  }

  getMensagens(): Observable<Mensagem[]> {
    return this.mensagensRemetente.valueChanges();
  }

}
