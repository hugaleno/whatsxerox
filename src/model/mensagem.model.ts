export class Mensagem {
    public tipo: string;

    constructor(
        public texto: string,
        public timestamp: any
    ){}
}