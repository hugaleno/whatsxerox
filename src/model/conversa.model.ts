export class Conversa{
    constructor(
        public titulo: string,
        public email: string,
        public ultimaMensagem: string,
        public timestamp: any
    ){}
}